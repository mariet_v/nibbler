//
// Snake.hh for nibbler in /home/mariet_v/celem/nibbler
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Wed Mar 26 15:12:58 2014 Mariette Valentin
// Last update Sat Apr  5 16:17:28 2014 Mariette Valentin
//

#ifndef SNAKE_HH_
# define SNAKE_HH_
#include <vector>

enum Direction
  {
    MTOP,
    MLEFT,
    MBOT,
    MRIGHT
  };

class Snake_Body
{
public:
  Snake_Body(int x, int y);
  ~Snake_Body();
  int	getPosX(void) const;
  int	getPosY(void) const;
  void	setPosX(int x);
  void	setPosY(int x);

private:
  int	_x;
  int	_y;
};

class Snake
{
public:
  Snake(int x, int y);
  ~Snake();
  void	add_body_part(int x, int y);
  std::vector<Snake_Body>& getBody(void);
  Direction getDirection(void) const;
  void	setDirection(Direction direction);
  
  private:
  std::vector<Snake_Body>	_body;
  Direction			_direction;
};

#endif
