
#ifndef IGRAPHICINTERFACE_HH_
# define IGRAPHICINTERFACE_HH_

class Game;

class IGraphicInterface
{
public:
  virtual void	init_lib(Game *game) = 0;
  virtual int	handle_events(Game *game) = 0;
  virtual void	refresh_window(Game *game) = 0;
  virtual void	close_lib(Game *game) = 0;
};

#endif
