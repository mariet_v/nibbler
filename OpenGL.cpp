//
// OpenGL.cpp for OpenGL.cpp in /home/sonnin_t/Travail/C++/Nibbler
// 
// Made by Tony.Sonnino
// Login   <sonnin_t@epitech.net>
// 
// Started on  Thu Apr  3 15:36:34 2014 Tony.Sonnino
// Last update Sun Apr  6 23:14:09 2014 Tony.Sonnino
//

#include "OpenGL.hh"

Game *g_game;

OpenGL::OpenGL()
{
}

OpenGL::~OpenGL()
{
}

void OpenGL::init_lib(Game *game)
{
  int	c = 0;
  g_game = game;
  this->sizex = game->getSizeX();
  this->sizey = game->getSizeY();
  glutInit(&c, NULL);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
  glutInitWindowSize(game->getSizeX()*30, game->getSizeY()*30);
  glutInitWindowPosition(0,0);
  glutCreateWindow("Nibbler");
  glViewport(0, 0, game->getSizeX()*30+30, game->getSizeY()*30+30);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, game->getSizeX()*30+30, 0, game->getSizeY()*30+30);
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glutKeyboardFunc(keyboard);
  glutSpecialFunc(skeyboard);
  glutIdleFunc(idle);
  glutDisplayFunc(render);
}

void OpenGL::draw_rect(int x1, int y1, int x2, int y2) const
{
  glRecti(x1, (this->sizex*30+30) - y1, x2, (this->sizey*30+30) - y2);
}

int OpenGL::handle_events(Game *game)
{
  glutMainLoopEvent();
  return (1);
}

void OpenGL::refresh_window(Game *game)
{
  int		i;
  int		j;

  j = 0;
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  while (j <= game->getSizeX())
    {
      i = -2;
      while (i < game->getSizeY())
	print_block(++i, j, game);
      j++;
    }
  glFlush();
  glutSwapBuffers();
  glutPostRedisplay();
}

void OpenGL::close_lib(Game *game)
{
}

void OpenGL::print_block(int i, int j, Game *game) const
{
  if (game->what_is_that(j, i) == SNAKE_BODY)
    {
      glColor3f(0.2f, 0.7f, 0.2f);
      draw_rect(j*30, i*30, j*30+30, i*30+30);
    }
  else if (game->what_is_that(j, i) == SNAKE_HEAD)
    {
      glColor3f(0.2f, 0.5f, 0.2f);
      draw_rect(j*30, i*30, j*30+30, i*30+30);
    }
  else if (game->what_is_that(j, i) == FRUIT)
    {
      glColor3f(0.8f, 0.1f, 0.1f);
      draw_rect(j*30, i*30, j*30+30, i*30+30);
    }
  else
    {
      glColor3f(0.1f, 0.1f, 0.8f);
      draw_rect(j*30, i*30, j*30+30, i*30+30);
    }
}

void keyboard(unsigned char key, int x, int y)
{
  (void) x;
  (void) y;

  switch (key)
    {
    case 27:
      exit(0);
    }
}

void skeyboard(int key, int x, int y)
{
  (void)x;
  (void)y;

  switch(key)
    {
    case GLUT_KEY_LEFT:
      g_game->snakeTurnLeft();
      break;
    case GLUT_KEY_RIGHT:
      g_game->snakeTurnRight();
      break;
    }
}

void idle(void)
{
  glutPostRedisplay();
}

void render(void)
{
  glutSwapBuffers();
}

void OpenGL::wait(void)
{
}

extern "C"
{
  IGraphicInterface* create_assistant()
  {
    return new OpenGL;
  }
}
