//
// Game.cpp for nibbler in /home/mariet_v/celem/nibbler
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Mon Mar 24 15:21:04 2014 Mariette Valentin
// Last update Sun Apr  6 23:21:36 2014 Mariette Valentin
//
#include <unistd.h>
#include <stdlib.h>
#include <ncurses.h>
#include "Game.hh"
#include "IGraphicInterface.hh"

Game::Game(int size_x, int size_y)
{
  _size_x = size_x;
  _size_y = size_y;
  _snake = new Snake(size_x / 2 - 2, size_y / 2);
  _fruit_pos_x = size_x / 4;
  _fruit_pos_y = size_y / 4;
  _snake->add_body_part(size_x / 2 - 1, size_y / 2);
  _snake->add_body_part(size_x / 2, size_y / 2);
  _snake->add_body_part(size_x / 2 + 1, size_y / 2);
  _lose = false;
  _score = 0;
  _head_direction = TOP;
}

Game::~Game()
{

}

int Game::getSizeX(void) const
{
  return (_size_x);
}

int Game::getSizeY(void) const
{
  return (_size_y);
}

int Game::getFruitPosX(void) const
{
  return (_fruit_pos_x);
}

int Game::getFruitPosY(void) const
{
  return (_fruit_pos_y);
}

int Game::setFruitPosX(int pos_x)
{
  _fruit_pos_x = pos_x;
}

int Game::setFruitPosY(int pos_y)
{
  _fruit_pos_y = pos_y;
}

int Game::loop(IGraphicInterface *GI)
{
  int	i = 0;

  while (_lose == false)
    {
      i++;
      usleep(500000);
      if (!(GI->handle_events(this)))
	return (1);
      move_snake();
      GI->refresh_window(this);
    }
}

void Game::snakeTurnRight(void)
{
  switch (_snake->getDirection())
    {
    case MTOP:
      _snake->setDirection(MRIGHT);
      _head_direction = RIGHT;
      break;

    case MLEFT:
      _snake->setDirection(MTOP);
      _head_direction = TOP;
      break;

    case MBOT:
      _snake->setDirection(MLEFT);
      _head_direction = LEFT;
      break;

    case MRIGHT:
      _snake->setDirection(MBOT);
      _head_direction = BOT;
      break;
    }
}

void Game::snakeTurnLeft(void)
{
  switch (_snake->getDirection())
    {
    case MTOP:
      _snake->setDirection(MLEFT);
      _head_direction = LEFT;
      break;

    case MLEFT:
      _snake->setDirection(MBOT);
      _head_direction = BOT;
      break;

    case MBOT:
      _snake->setDirection(MRIGHT);
      _head_direction = RIGHT;
      break;

    case MRIGHT:
      _snake->setDirection(MTOP);
      _head_direction = TOP;
      break;
    }
}

void Game::move_snake(void)
{
  int	i;
  int	x;
  int	y;

  i = _snake->getBody().size() - 1;
  x = _snake->getBody()[i].getPosX();
  y = _snake->getBody()[i].getPosY();
  while (i > 0)
    {
      _snake->getBody()[i].setPosX(_snake->getBody()[i - 1].getPosX());
      _snake->getBody()[i].setPosY(_snake->getBody()[i - 1].getPosY());
      i--;
    }
  switch (_snake->getDirection())
    {
    case MTOP:
      if (_snake->getBody()[0].getPosY() > 0 && what_is_that(_snake->getBody()[0].getPosX(), _snake->getBody()[0].getPosY() - 1) != SNAKE_BODY)
	_snake->getBody()[0].setPosY(_snake->getBody()[0].getPosY() - 1);
      else
	_lose = true;
      break;

    case MLEFT:
      if (_snake->getBody()[0].getPosX() > 0 && what_is_that(_snake->getBody()[0].getPosX() - 1, _snake->getBody()[0].getPosY()) != SNAKE_BODY)
	_snake->getBody()[0].setPosX(_snake->getBody()[0].getPosX() - 1);
      else
	_lose = true;
      break;

    case MBOT:
      if (_snake->getBody()[0].getPosY() < _size_y - 1 && what_is_that(_snake->getBody()[0].getPosX(), _snake->getBody()[0].getPosY() + 1) != SNAKE_BODY)
	_snake->getBody()[0].setPosY(_snake->getBody()[0].getPosY() + 1);
      else
	_lose = true;
      break;

    case MRIGHT:
      if (_snake->getBody()[0].getPosX() < _size_x - 1 && what_is_that(_snake->getBody()[0].getPosX() + 1, _snake->getBody()[0].getPosY()) != SNAKE_BODY)
	_snake->getBody()[0].setPosX(_snake->getBody()[0].getPosX() + 1);
      else
	_lose = true;
      break;
    }
  if (what_is_that(_snake->getBody()[0].getPosX(), _snake->getBody()[0].getPosY()) == FRUIT)
    {
      if (_score == 0)
	_score = 1;
      else
	_score *= 2;
      _snake->add_body_part(x, y);
      put_new_fruit();
    }
}

CaseType Game::what_is_that(int x, int y) const
{
  int	i;

  i = 0;
  if (x == _fruit_pos_x && y == _fruit_pos_y)
    return (FRUIT);
  while (i < _snake->getBody().size())
    {
      if (_snake->getBody()[0].getPosX() == x && _snake->getBody()[0].getPosY() == y)
	return (SNAKE_HEAD);
      if (_snake->getBody()[i].getPosX() == x && _snake->getBody()[i].getPosY() == y)
	return (SNAKE_BODY);
      i++;
    }
  return (EMPTY);
}

void Game::recap_game(void) const
{
  std::cout << "GAME OVER" << std::endl << "SCORE : " << _score*10 << std::endl;
}


void Game::put_new_fruit(void)
{
  int	x;
  int	y;

  if (_snake->getBody().size() == (_size_x * _size_y))
    {
      _lose = true;
      return;
    }
  x = (rand() % _size_x);
  y = (rand() % _size_y);
  while (what_is_that(x, y) != EMPTY)
    {
      x = (rand() % _size_x);
      y = (rand() % _size_y);
    }
  _fruit_pos_x = x;
  _fruit_pos_y = y;
}

HeadDirection Game::getHeadDirection(void) const
{
  return (_head_direction);
}
