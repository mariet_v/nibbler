//
// Snake.cpp for nibbler in /home/mariet_v/celem/nibbler
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Wed Mar 26 15:27:01 2014 Mariette Valentin
// Last update Sat Apr  5 16:18:39 2014 Mariette Valentin
//

#include "Snake.hh"

Snake_Body::Snake_Body(int x, int y)
{
  _x = x;
  _y = y;
}

Snake_Body::~Snake_Body()
{

}

int Snake_Body::getPosX(void) const
{
  return (_x);
}

int Snake_Body::getPosY(void) const
{
  return (_y);
}

void Snake_Body::setPosX(int x)
{
  _x = x;
}

void Snake_Body::setPosY(int y)
{
  _y = y;
}

Snake::Snake(int x, int y)
{
  _body.push_back(Snake_Body(x, y));
  _direction = MTOP;
}

Snake::~Snake()
{

}

void Snake::add_body_part(int x, int y)
{
  _body.push_back(Snake_Body(x, y));
}

std::vector<Snake_Body>& Snake::getBody(void)
{
  return _body;
}

Direction Snake::getDirection(void) const
{
  return (_direction);
}

void Snake::setDirection(Direction direction)
{
  _direction = direction;
}
