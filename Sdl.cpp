//
// Sdl.cpp for Sdl.cpp in /home/sonnin_t/Travail/C++/Nibbler
// 
// Made by Tony.Sonnino
// Login   <sonnin_t@epitech.net>
// 
// Started on  Thu Apr  3 15:36:34 2014 Tony.Sonnino
// Last update Sun Apr  6 22:51:09 2014 Pierre Palomino
//

#include "Sdl.hh"

Sdl::Sdl()
{

}

Sdl::~Sdl()
{
}

void Sdl::init_lib(Game *game)
{
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0)
    {
      std::cerr << "Fail of SDL initialization." << std::endl;
      exit(-1);
    }
  this->pWindow = SDL_CreateWindow("Nibbler", SDL_WINDOWPOS_UNDEFINED,
				   SDL_WINDOWPOS_UNDEFINED, game->getSizeX()*30,
				   game->getSizeY()*30, SDL_WINDOW_SHOWN);
  this->background = SDL_LoadBMP("background.bmp");
  this->head = SDL_LoadBMP("head.bmp");
  this->body = SDL_LoadBMP("body.bmp");
  this->fruit = SDL_LoadBMP("fruit.bmp");
}

int Sdl::handle_events(Game *game)
{
  while (SDL_PollEvent(&this->event))
    {
      if (this->event.type == SDL_QUIT)
	{
	  SDL_Quit();
	  return (0);
	}
      if (this->event.type == SDL_KEYDOWN)
	{
	  if (this->event.key.keysym.sym == SDLK_ESCAPE)
	    {
	      SDL_Quit();
	      return (0);
	    }
	  if (this->event.key.keysym.sym == SDLK_LEFT)
	    game->snakeTurnLeft();
	  if (this->event.key.keysym.sym == SDLK_RIGHT)
	    game->snakeTurnRight();
	}
    }
  return (1);
}

void Sdl::refresh_window(Game *game)
{
  SDL_Rect	dest;
  int		i;
  int		j;

  j = 0;
  while (j < game->getSizeX())
    {
      i = -1;
      while (i < game->getSizeY())
	print_block(++i, j, dest, game);
      j++;
    }
  SDL_UpdateWindowSurface(this->pWindow);
}

void Sdl::close_lib(Game *game)
{
  SDL_Quit();
}

void Sdl::print_block(int i, int j, SDL_Rect dest, Game *game) const
{
  dest.x = j * 30;
  dest.y = i * 30;
  if (game->what_is_that(j, i) == SNAKE_BODY)
    SDL_BlitSurface(this->body, NULL, SDL_GetWindowSurface(this->pWindow), &dest);
  else if (game->what_is_that(j, i) == SNAKE_HEAD)
    SDL_BlitSurface(this->head, NULL, SDL_GetWindowSurface(this->pWindow), &dest);
  else if (game->what_is_that(j, i) == FRUIT)
    SDL_BlitSurface(this->fruit, NULL, SDL_GetWindowSurface(this->pWindow), &dest);
  else
    SDL_BlitSurface(this->background, NULL, SDL_GetWindowSurface(this->pWindow), &dest);
}

extern "C"
{
  IGraphicInterface* create_assistant()
  {
    return new Sdl();
  }
}
