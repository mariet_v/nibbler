//
// Ncurses.cpp for nibbler in /home/mariet_v/celem/nibbler
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Wed Mar 26 16:17:50 2014 Mariette Valentin
// Last update Sun Apr  6 23:26:27 2014 Mariette Valentin
//

#include "Ncurses.hh"

Ncurses::Ncurses()
{

}

Ncurses::~Ncurses()
{

}

void Ncurses::init_lib(Game *game)
{
  initscr();
  raw();
  keypad(stdscr, TRUE);
  noecho();
  nodelay(stdscr,TRUE);
  curs_set(0);
}

int Ncurses::handle_events(Game *game)
{
  int ch = 0;

  while ((ch = getch()) != -1)
    {
      if (ch == 'q' || ch == 27)
	return (0);
      else if (ch == 261)
	game->snakeTurnRight();
      else if (ch == 260)
	game->snakeTurnLeft();
    }
}

void Ncurses::refresh_window(Game *game)
{
  int	i = 0;
  int	j = 0;

  clear();
  print_wall(game);
  while (i < game->getSizeY())
    {
      j = 0;
      while (j < game->getSizeX())
	{
	  print_char(game->what_is_that(j, i), j + 1, i + 1, game->getHeadDirection());
	  j++;
	}
      i++;
    }
  mvprintw(0, 0, "Nibbler");
  refresh();
}

void Ncurses::close_lib(Game *game)
{
  endwin();
}

void Ncurses::print_char(CaseType type, int x, int y, HeadDirection direction) const
{
  if (type == SNAKE_BODY)
    mvaddch(y, x, '*');
  else if (type == FRUIT)
    mvaddch(y, x, 'o');
  else if (type == SNAKE_HEAD)
    {
	switch (direction)
	  {
	  case TOP:
	    mvaddch(y, x, '^');
	    break;

	  case RIGHT:
	     mvaddch(y, x, '>');
	     break;

	  case BOT:
	     mvaddch(y, x, 'v');
	     break;

	  case LEFT:
	     mvaddch(y, x, '<');
	     break;
	  }
    }
}

void Ncurses::print_wall(Game *game) const
{
  int	i = 0;

  while (i <= game->getSizeX())
    mvaddch(0, i++, '-');
  i = 0;
  while (i <= game->getSizeY())
    mvaddch(i++, 0, '|');
  i = 0;
  while (i <= game->getSizeY())
    mvaddch(i++, game->getSizeX() + 1, '|');
  i = 0;
  while (i <= game->getSizeX() + 1)
    mvaddch(game->getSizeY() + 1, i++, '-');
}

extern "C"
{
  IGraphicInterface* create_assistant()
  {
    return new Ncurses();
  }
}
