//
// main.cpp for nibbler in /home/mariet_v/celem/nibbler
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Mon Mar 24 15:04:18 2014 Mariette Valentin
// Last update Sun Apr  6 23:19:12 2014 Tony.Sonnino
//

#include <iostream>
#include <cstdlib>
#include <dlfcn.h>
#include "IGraphicInterface.hh"
#include "Game.hh"

int main(int ac, char** argv)
{
  if (ac < 4)
    {
      std::cerr << "Usage : ./nibbler X Y <libXXX.so>" << std::endl;
      return(1);
    }
  
  IGraphicInterface* (*external_creator)();
  void* dlhandle;
  
  dlhandle = dlopen(argv[3], RTLD_LAZY);
  if (dlhandle == NULL)
    {
      std::cerr << "Wrong library name." << std::endl;
      return(1);
    }
  external_creator = reinterpret_cast<IGraphicInterface* (*)()>(dlsym(dlhandle, "create_assistant"));
  if (external_creator == NULL)
    {
      std::cerr << "Error when trying to use graphic library." << std::endl;
      return (-1);
    }
  IGraphicInterface* GI = external_creator();
  if (atoi(argv[1]) < 5 || atoi(argv[2]) < 5)
    {
      std::cout << "Choose a bigger map." << std::endl;
      return (0);
    }
  Game game(atoi(argv[1]), atoi(argv[2]));

  GI->init_lib(&game);
  game.loop(GI);
  GI->close_lib(&game);
  dlclose(dlhandle);
  game.recap_game();
  return (0);	
}
