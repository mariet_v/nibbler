//
// Sdl.hh for Sdl.hh in /home/sonnin_t/Travail/C++/Nibbler
// 
// Made by Tony.Sonnino
// Login   <sonnin_t@epitech.net>
// 
// Started on  Thu Apr  3 15:24:05 2014 Tony.Sonnino
// Last update Sun Apr  6 22:48:26 2014 Pierre Palomino
//

#ifndef SDL_HH_
# define SDL_HH_

#include <SDL2/SDL.h>
#include "Game.hh"
#include "IGraphicInterface.hh"

class Sdl : public IGraphicInterface
{
public:
  Sdl();
  ~Sdl();
  virtual void	init_lib(Game *game);
  virtual int	handle_events(Game *game);
  virtual void	refresh_window(Game *game);
  virtual void	close_lib(Game *game);
  virtual void	print_block(int i, int j, SDL_Rect dest, Game *game) const;

private:
  SDL_Window*	pWindow;
  SDL_Surface*	background;
  SDL_Surface*	head;
  SDL_Surface*	body;
  SDL_Surface*	fruit;
  SDL_Event	event;
};

#endif
