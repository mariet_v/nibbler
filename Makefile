NAME_EXE = nibbler
SRC_EXE = main.cpp \
	Game.cpp \
	Snake.cpp
OBJ_EXE = $(SRC_EXE:.cpp=.o)

NAME_LIB_NCURSES = lib_nibbler_ncurses.so
SRC_LIB_NCURSES		= Ncurses.cpp \
			Game.cpp \
			Snake.cpp
OBJ_LIB_NCURSES		= $(SRC_LIB_NCURSES:.cpp=.o)

NAME_LIB_SDL = lib_nibbler_sdl.so
SRC_LIB_SDL		= Sdl.cpp \
			Game.cpp \
			Snake.cpp
OBJ_LIB_SDL		= $(SRC_LIB_SDL:.cpp=.o)

NAME_LIB_OGL = lib_nibbler_ogl.so
SRC_LIB_OGL		= OpenGL.cpp \
			Game.cpp \
			Snake.cpp
OBJ_LIB_OGL		= $(SRC_LIB_OGL:.cpp=.o)

LDFLAGS += -ldl -lncurses -Wl,-rpath,/usr/local/lib64 -L/usr/local/lib64 -lSDL2 -lglut -lGL -lGLU
CXXFLAGS += -fPIC

CXX = g++

all: $(NAME_EXE) $(NAME_LIB_NCURSES) $(NAME_LIB_SDL) $(NAME_LIB_OGL)

$(NAME_EXE): $(OBJ_EXE)
	$(CXX) -o $(NAME_EXE) $(OBJ_EXE) $(LDFLAGS)

$(NAME_LIB_NCURSES): $(OBJ_LIB_NCURSES)
	$(CXX) -shared -o $(NAME_LIB_NCURSES) $(OBJ_LIB_NCURSES)

$(NAME_LIB_SDL): $(OBJ_LIB_SDL)
	$(CXX) -shared -o $(NAME_LIB_SDL) $(OBJ_LIB_SDL)

$(NAME_LIB_OGL): $(OBJ_LIB_OGL)
	$(CXX) -shared -o $(NAME_LIB_OGL) $(OBJ_LIB_OGL)

clean:
	rm -f $(OBJ_LIB) $(OBJ_EXE) $(OBJ_LIB2) $(OBJ_LIB_NCURSES) $(OBJ_LIB_SDL) $(OBJ_LIB_OGL)

fclean: clean
	rm -f $(NAME_EXE) $(NAME_LIB_NCURSES) $(NAME_LIB_SDL) $(NAME_LIB_OGL)

re: fclean all
