//
// Game.hh for nibbler in /home/mariet_v/celem/nibbler
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Mon Mar 24 15:17:59 2014 Mariette Valentin
// Last update Sun Apr  6 23:23:22 2014 Mariette Valentin
//

#ifndef GAME_HH_
# define GAME_HH_
#include <iostream>
#include "Snake.hh"

class IGraphicInterface;

enum HeadDirection
  {
    TOP,
    RIGHT,
    BOT,
    LEFT
  };

enum CaseType
  {
    EMPTY,
    FRUIT,
    SNAKE_BODY,
    SNAKE_HEAD
  };

class Game
{
public:
  Game(int size_x, int size_y);
  ~Game();
  int		getSizeX(void) const;
  int		getSizeY(void) const;
  int		getFruitPosX(void) const;
  int		getFruitPosY(void) const;
  int		setFruitPosX(int pos_x);
  int		setFruitPosY(int pos_y);
  int		loop(IGraphicInterface *GI);
  void		snakeTurnRight(void);
  void		snakeTurnLeft(void);
  void		move_snake(void);
  CaseType	what_is_that(int x, int y) const;
  void		recap_game(void) const;
  void		put_new_fruit(void);
  HeadDirection	getHeadDirection(void) const;

private:
  int			_size_x;
  int			_size_y;
  int			_fruit_pos_x;
  int			_fruit_pos_y;
  HeadDirection      	_head_direction;
  Snake			*_snake;
  bool			_lose;
  int			_score;
};

#endif
