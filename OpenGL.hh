//
// opengl.hh for opengl in /home/palomi_p/Projets/cpp/cpp_nibbler/pierre
// 
// Made by Pierre Palomino
// Login   <palomi_p@epitech.net>
// 
// Started on  Fri Apr  4 16:19:14 2014 Pierre Palomino
// Last update Sun Apr  6 22:48:07 2014 Pierre Palomino
//

#ifndef OPENGL_HH_
# define OPENGL_HH_

#include <iostream>
#include <GL/freeglut.h>
#include "Game.hh"
#include "IGraphicInterface.hh"

class OpenGL : public IGraphicInterface
{
public:
  OpenGL();
  ~OpenGL();
  virtual void  init_lib(Game *game);
  virtual int   handle_events(Game *game);
  virtual void  refresh_window(Game *game);
  virtual void  close_lib(Game *game);
  virtual void	wait(void);
  virtual void	print_block(int i, int j, Game *game) const;
  void	draw_rect(int x1, int y1, int x2, int y2) const;
private:
  int	sizex;
  int	sizey;
};

void keyboard(unsigned char key, int x, int y);
void skeyboard(int, int, int);
void render(void);
void idle(void);

#endif
