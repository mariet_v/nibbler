//
// Ncurses.hh for nibbler in /home/mariet_v/celem/nibbler
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Wed Mar 26 16:11:59 2014 Mariette Valentin
// Last update Sun Apr  6 23:24:24 2014 Mariette Valentin
//

#ifndef NCURSES_HH_
# define NCURSES_HH_
#include <ncurses.h>
# include "Game.hh"
# include "IGraphicInterface.hh"

class Ncurses : public IGraphicInterface
{
public:
  Ncurses();
  ~Ncurses();
  virtual void	init_lib(Game *game);
  virtual int	handle_events(Game *game);
  virtual void	refresh_window(Game *game);
  virtual void	close_lib(Game *game);
  virtual void	print_char(CaseType type, int x, int y, HeadDirection direction) const;
  virtual void	print_wall(Game *game) const;
};
#endif
